import React, { useState } from 'react';
import { Link } from "react-router-dom";
import './App.css';

function Home(props) {
  const [counter, setCounterValue] = useState(0);

  return (
    <div className="App">
      <header className="App-header">

      <input type="text" name="username" onChange={(e) => {
        props.setUsername(e.target.value);
        setCounterValue(counter + 1);
      }} />
      <Link
        className="App-link"
        to="/show"
      >
        Show
      </Link>
      <br />
      Counter : {counter}
      </header>
    </div>
  );
}

export default Home;

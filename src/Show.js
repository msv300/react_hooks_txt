import React from 'react';
import './App.css';

function Show(props) {
  return (
    <div className="App">
      <header className="App-header">
        Show Page
        <br />
      Your Username is : {props.username}
      </header>
    </div>
  );
}

export default Show;

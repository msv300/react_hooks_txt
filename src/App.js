import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';
import Home from "./Home";
import Show from "./Show";

function App() {
  const [username, setUsername] = useState('');

  return (
    <Router>
      <Route path="/" exact render={() => <Home setUsername={setUsername} />} />
      <Route path="/show" component={() => <Show username={username} />} />
    </Router>
  );
}

export default App;
